/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 19:29:16 by abnaceur          #+#    #+#             */
/*   Updated: 2016/11/11 19:35:37 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#define BUF_SIZE 4096

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		main(int argc, char **argv)
{
	int		fd;
	char	buf[BUF_SIZE + 1];

	if (argc > 2)
	{
		ft_putstr("Too many arguments.");
		ft_putchar('\n');
	}
	else if (argc == 1)
	{
		ft_putstr("File name missing.");
		ft_putchar('\n');
	}
	else if (argc == 2)
	{
		fd = open(argv[1], O_RDONLY);
		while (read(fd, &buf, 1) != 0)
			write(1, &buf, 1);
		close(fd);
	}
	return (0);
}
