/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 09:46:55 by abnaceur          #+#    #+#             */
/*   Updated: 2016/09/08 09:55:50 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int		*tmp_tab;
	int		index;

	index = 0;
	if (min >= max)
		return (0);
	tmp_tab = (int*)malloc(sizeof(tmp_tab) * (max - min));
	while (min < max)
	{
		tmp_tab[index] = min;
		min++;
		index++;
	}
	return (tmp_tab);
}
