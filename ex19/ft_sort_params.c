/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 17:37:42 by abnaceur          #+#    #+#             */
/*   Updated: 2016/11/10 11:35:46 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while ((s1[i] != '\0') && (s2[i] != '\0') && (s1[i] == s2[i]))
		i++;
	return (s1[i] - s2[i]);
}

void	ft_print_params(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_sort_params(int argc, char **argv)
{
	int		i;
	char	*tmp;
	int		s;

	s = 1;
	while (s)
	{
		s = 0;
		i = 0;
		while (++i < argc - 1)
		{
			if (ft_strcmp(argv[i], argv[i + 1]) > 0)
			{
				tmp = argv[i];
				argv[i] = argv[i + 1];
				argv[i + 1] = tmp;
				s = 1;
			}
		}
	}
	return (0);
}

int		main(int argc, char **argv)
{
	int i;

	ft_sort_params(argc, argv);
	i = 0;
	while (++i < argc)
	{
		ft_print_params(argv[i]);
		ft_putchar('\n');
	}
	return (0);
}
