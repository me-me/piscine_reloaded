/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 19:45:16 by abnaceur          #+#    #+#             */
/*   Updated: 2016/11/11 19:49:54 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int i;
	int resu;
	int temp;

	resu = 0;
	temp = 0;
	i = 0;
	while (i < nb)
	{
		if (temp == nb)
		{
			return (resu);
			i = nb + 1;
		}
		else if (temp != nb)
		{
			temp = i * i;
			resu = i;
			i++;
		}
	}
	return (0);
}
