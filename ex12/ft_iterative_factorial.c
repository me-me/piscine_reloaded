/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 15:18:09 by abnaceur          #+#    #+#             */
/*   Updated: 2016/11/10 12:21:34 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int i;
	int rst;

	i = 1;
	rst = 1;
	if ((nb < 0) || (nb > 12))
		return (0);
	else if ((nb == 0) || (nb == 1))
		return (1);
	else
		while (i <= nb)
		{
			rst = rst * i;
			i++;
		}
	return (rst);
}
