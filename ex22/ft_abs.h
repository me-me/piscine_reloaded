/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 17:56:43 by abnaceur          #+#    #+#             */
/*   Updated: 2016/11/11 18:03:56 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FT_ABS_H
# define __FT_ABS_H
# define ABS(x) (x > 0 ? x : -x)
#endif
