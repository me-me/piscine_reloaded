/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 10:38:01 by abnaceur          #+#    #+#             */
/*   Updated: 2016/11/11 20:21:47 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		i;
	int		len;
	char	*nw_str;

	i = 0;
	len = 0;
	while (src[len])
		len++;
	nw_str = (char*)malloc(sizeof(*nw_str) * (len + 1));
	if (nw_str == NULL)
		return (NULL);
	while (i < len)
	{
		nw_str[i] = src[i];
		i++;
	}
	nw_str[len] = '\0';
	return (nw_str);
}
